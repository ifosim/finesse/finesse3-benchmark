from finesse_benchmark import get_benchmarks, set_finesse_repo

# uncomment if necessary, default is ~/src/finesse3
# set_finesse_repo("/path/to/finesse/git/repo")

benchmarks = get_benchmarks()

# refer by git references

benchmarks["origin/fix/IGWN-conda-pipeline"].compare(benchmarks["develop"], svg_fn="references.svg")

# refer by commit hashes

benchmarks["77bc39cc52c1af65cd4cd9d84cfe514b03ea9a14"].compare(
    benchmarks["f615c03d53aeaf5f56a4a6c9a57aede15f97f16b"], svg_fn="hashes.svg"
)

# use custom plot labels

benchmarks["77bc39cc52c1af65cd4cd9d84cfe514b03ea9a14"].with_name("slow_branch").compare(
    benchmarks["f615c03d53aeaf5f56a4a6c9a57aede15f97f16b"].with_name("fast_branch"), svg_fn="custom_labels.svg"
)