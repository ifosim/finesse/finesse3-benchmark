# finesse3-benchmark

Repo to collect benchmarks from the finesse3 performance test jobs, collect them
and possibly add some scripting for plots and stuff.

See <https://pytest-benchmark.readthedocs.io/en/latest/> for example commands

## Python script for ease of use

You can use/edit the `finesse_benchmark.py` script in this repo to more easily
compare benchmarks of different commits. For now it needs the `pygal` dependency
and it's not a package, so you would need to place your scripts/notebooks in
this folder to use it.

First pull the latest benchmarks form the repo

```bash
git pull
```

then compare two commits by commit hash or by name.

The script uses `git` to translate commit hashes into git refs like `develop` or
`feature/polarisation`. It assumes it's located in `~/src/finesse3`, otherwise
you need to call `set_finesse_repo` to the correct directory. You might need to
`git fetch` in your finesse repo to pull the latest references from the repo,
otherwise the `develop` alias might point to an outdated commit

```python
from finesse_benchmark import get_benchmarks, set_finesse_repo

# uncomment if necessary, default is ~/src/finesse3
# set_finesse_repo("/path/to/finesse/git/repo")

benchmarks = get_benchmarks()

# refer by git references

benchmarks["origin/fix/IGWN-conda-pipeline"].compare(benchmarks["develop"])
```

![git_references](references.svg)

```python
# refer by commit hashes

benchmarks["77bc39cc52c1af65cd4cd9d84cfe514b03ea9a14"].compare(
    benchmarks["f615c03d53aeaf5f56a4a6c9a57aede15f97f16b"]
)
```

![commit hashes](hashes.svg)

```python
# use custom plot labels

benchmarks["77bc39cc52c1af65cd4cd9d84cfe514b03ea9a14"].with_name("slow_branch").compare(
    benchmarks["f615c03d53aeaf5f56a4a6c9a57aede15f97f16b"].with_name("fast_branch")
)
```

![custom labels](custom_labels.svg)
