from __future__ import annotations

import os
import re
import shutil
import subprocess
import tempfile
import webbrowser
from dataclasses import dataclass
from pathlib import Path

from packaging import version

try:
    import pygal
except ImportError:
    raise ImportError("Benchmark comparison utility needs pygal module!")

FINESSE_REPO = Path().home() / "src" / "finesse3"
BENCHMARK_FOLDER = Path(__file__).parent / ".benchmarks"


class FinesseRepoNotFoundError(Exception):
    pass


def set_finesse_repo(path: Path | str):
    global FINESSE_REPO

    path = Path(path)
    assert path.exists() and path.is_dir()
    FINESSE_REPO = path


def git_command(command: str) -> str:
    if (
        subprocess.run(
            "git status", cwd=FINESSE_REPO, capture_output=True, shell=True
        ).returncode
        != 0
    ):
        raise FinesseRepoNotFoundError(
            f"Finesse3 repo not found at '{FINESSE_REPO}', run 'set_finesse_repo' to fix"
        )
    return subprocess.run(
        command, cwd=FINESSE_REPO, text=True, shell=True, capture_output=True
    ).stdout


def in_ipython():
    """Checks whether the current script is running under IPython.

    Returns
    -------
    bool
        True if __IPYTHON__ is defined, otherwise False.
    """
    try:
        __IPYTHON__
    except NameError:
        return False
    else:
        return True


def display_svg(fn: Path):
    if in_ipython():
        from IPython.display import SVG

        return SVG(Path(fn))
    else:
        # windows probably does not have 'open' command
        if os.name == "nt":
            webbrowser.open(f"file://{fn.name}")
        else:
            subprocess.run(f"open {fn}", check=True, shell=True)


@dataclass
class Benchmark:
    python_version: version.Version
    platform: str
    commit: str
    refs: list[str]
    json: Path
    name: str | None = None

    @property
    def pretty_name(self) -> str:
        if self.name:
            return self.name + f"[{self.commit[:5]}]"
        elif len(self.refs):
            return self.refs[0] + f"[{self.commit[:5]}]"
        else:
            return self.commit

    @classmethod
    def from_path(cls: type[Benchmark], path: Path) -> Benchmark:
        parent = path.parent.parts[-1]
        python_version = version.parse(re.findall(r"\d.\d{2}", parent)[0])
        platform = parent.split("-")[0]
        commit = path.stem.split("_")[1]
        # we cut off refs/heads, refs/tags/ and refs/remotes from the refs, assuming there are no duplicates in these categories
        # e.g. a branch name identical to a tag name
        refs = git_command(
            f"git show-ref | grep {commit} | sed -e 's@refs/heads/@@g' -e s@refs/tags/@@g -e s@refs/remotes/@@g | cut -d ' ' -f2"
        ).split()
        return cls(
            python_version=python_version,
            platform=platform,
            commit=commit,
            refs=refs,
            json=path,
            name=refs[0].split("/")[-1] if len(refs) else None,
        )

    def with_name(self, name: str) -> Benchmark:
        """Return a this benchmark with a custom name, which will be used in the labels of the svg plot

        Parameters
        ----------
        name : str
            Custom name

        Returns
        -------
        Benchmark
            This benchmark with the name adjusted
        """
        self.name = name
        return self

    def compare(self, other: Benchmark, svg_fn: Path | str | None = None, title: str | None = None):
        """Compare this benchmark against another benchmark. Creates and
        displays an SVG image with the comparison.

        By default the first git ref for the benchmark commit will be used as a
        plot label, otherwise it will show the commit hash.

        To use a custom name, use ``Benchmark.with_name(custom_name).compare``

        Parameters
        ----------
        other : Benchmark
            Benchmark to compare against
        svg_fn : Path | str | None, optional
            Filename for the svg, by default None, which will save it /tmp/
        """
        files = [self.json, other.json]
        if (self.name, other.name) != (None, None):
            assert self.name != other.name
        with tempfile.TemporaryDirectory(delete=False) as tmpdir:
            for i, benchmark in enumerate([self, other]):
                if benchmark.name is None:
                    files[i] = Path(tmpdir) / f"{benchmark.json.parts[-1].replace('0001_', '')}"
                else:
                    files[i] = Path(tmpdir) / f"{benchmark.name}.json"
                shutil.copy(benchmark.json, files[i])

            if not svg_fn:
                svg_fn = Path(tmpdir) / "histogram"
            command = f"pytest-benchmark compare {files[0]} {files[1]} --histogram {Path(svg_fn).stem} --sort name"
            print(command)
            subprocess.run(command, check=True, shell=True, capture_output=True)

            with open(svg_fn, 'r') as f:
                contents = f.read()
            old_title = "Speed in Milliseconds (ms)"
            new_title = f"'{self.pretty_name}' vs '{other.pretty_name}'" if title is None else title
            contents = contents.replace(old_title, new_title)
            contents = contents.replace("Duration", "Duration [ms]")
            with open(svg_fn, 'w') as f:
                f.write(contents)

            display_svg(Path(svg_fn).with_suffix(".svg"))


def get_benchmarks() -> dict[str, Benchmark]:
    benchmarks: dict[str, Benchmark] = {}
    for path in BENCHMARK_FOLDER.rglob("*.json"):
        benchmark = Benchmark.from_path(path)
        benchmarks[benchmark.commit] = benchmark
        for ref in benchmark.refs:
            benchmarks[ref] = benchmark
    return benchmarks
